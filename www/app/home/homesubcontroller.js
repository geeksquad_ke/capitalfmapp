angular.module('home')
    .controller('HomeSubController', ['$scope', 'HomeService', '$ionicLoading', '$stateParams', '$ionicPopup', '$ionicNavBarDelegate', function ($scope, HomeService, $ionicLoading, $stateParams, $ionicPopup, $ionicNavBarDelegate) {

        init();
        function init() {

            $scope.maincategory = $stateParams.maincategory;
            $scope.subcategory = $stateParams.subcategory;


            $ionicLoading.show({
                template: 'Loading...'
            });

            HomeService.getFeeds($scope.maincategory, $scope.subcategory)
                .then(function (response) {
                    $scope.posts = response.data;
                }, function (error) {

                    var showAlert = function () {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Error Occured',
                            template: 'The information is not available or you do not have a working internet connection'
                        });

                        alertPopup.then(function (res) {
                            $ionicNavBarDelegate.back();
                        });
                    };

                    showAlert();
                });
            $ionicLoading.hide();


        }


    }]);
