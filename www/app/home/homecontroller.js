angular.module('home')
    .controller('HomeController', ['$scope', 'HomeService', '$ionicLoading', '$ionicPopup', function ($scope, HomeService, $ionicLoading, $ionicPopup) {


        init();


        function init() {

            getContent();

        }


        function getContent() {

            $ionicLoading.show({
                template: 'Loading...'
            });

            $scope.mainCategories = HomeService.getMainCategories();

            //fetch sub categories for each main category
            angular.forEach($scope.mainCategories, function (value, key) {
                HomeService.getChildCategories(value.slug)
                    .then(function (res) {
                        $scope.mainCategories[key].childCategories = res;
                        //Hide loading animation if its in the last category
                        if ($scope.mainCategories.length - 1 == key) {
                            $ionicLoading.hide();
                        }
                    }, function(error){

                        var showAlert = function() {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Something went wrong!',
                                template: 'Can\'t Fetch Data, Kindly confirm that you have a working internet connection'
                            });

                            alertPopup.then(function(res) {
                                init();
                            });
                        };

                        $ionicLoading.hide();
                        showAlert();

                    })
            });
        }

        $scope.shouldShowDelete = true;
        $scope.shouldShowReorder = true;
        $scope.listCanSwipe = true;

    }]);
