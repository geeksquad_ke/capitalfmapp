angular.module('home')
    .factory('HomeService', ['$http', 'API_URL', 'API_KEY', 'CacheFactory', '$q', '$timeout', function ($http, API_URL, API_KEY, CacheFactory, $q, $timeout) {

        var homeDataCache = CacheFactory('homeDataCache', {
            maxAge: 1 * 60 * 60 * 1000, // Items added to this cache expire after 1 hour.
            //cacheFlushInterval: 12 * 60 * 60 * 1000, // This cache will clear itself every 12 hours.
            //deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
            storageMode: 'localStorage' // This cache will use `localStorage`.
        });


        var httpConfig = {
            timeout: 5000
        };


        var homeData = {
            getMainCategories: getMainCategories,
            getChildCategories: getChildCategories,
            getFeeds: getFeeds,
            getPost: getPost
        };

        return homeData;


        function getMainCategories() {
            var categories = [
                {
                    name: 'News',
                    slug: 'news'
                },
                {
                    name: 'Business',
                    slug: 'business'
                },
                {
                    name: 'Lifestyle',
                    slug: 'lifestyle'
                },
                {
                    name: 'Sports',
                    slug: 'sports'
                }];
            return categories;
        }


        function getChildCategories(mainCategory, refresh) {


            var deferred = $q.defer();

            var cacheId = 'childCategories/' + mainCategory;

            var childCategoriesCache = homeDataCache.get(cacheId);

            refresh = childCategoriesCache || false;

            if (!childCategoriesCache) {
                $http.get(API_URL + mainCategory + '/wp-json/wp/v2/categories', httpConfig)
                    .then(function (res) {

                        homeDataCache.put(cacheId, res.data);
                        deferred.resolve(res.data);
                    }, function (err) {
                        deferred.reject(err);
                    });

            } else if (childCategoriesCache && refresh == true){

                $http.get(API_URL + mainCategory + '/wp-json/wp/v2/categories', httpConfig)
                    .then(function (res) {
                        homeDataCache.put(cacheId, res.data);
                        deferred.resolve(res.data);
                    }, function (err) {
                        deferred.resolve(childCategoriesCache);

                    });

            } else {
                console.log(childCategoriesCache);
                deferred.resolve(childCategoriesCache.data);

            }
            return deferred.promise;


        }


        function getFeeds(mainCategory, subCategory) {

            var deferred = $q.defer();

            var cacheId = 'feeds/' + mainCategory + '/' + subCategory;

            var feedsCache = homeDataCache.get(cacheId);

            if(feedsCache){
                deferred.resolve(feedsCache);
            }

            else{
                $http.get(API_URL + mainCategory + '/wp-json/wp/v2/posts?categories=' + subCategory, httpConfig)
                    .then(function(data){
                        homeDataCache.put(cacheId, data);
                        deferred.resolve(data);
                    }, function(err){
                        deferred.reject(err);
                    });
            }

            return deferred.promise;

        }

        function getPost(mainCategory, id) {

            var deferred = $q.defer();

            var cacheId = mainCategory + '/posts/' + id;

            var postCache = homeDataCache.get(cacheId);

            if (postCache) {
                deferred.resolve(postCache);
            } else {
                $http.get('https://www.capitalfm.co.ke/' + mainCategory + '/wp-json/wp/v2/posts/' + id, httpConfig)
                    .then(function (data) {

                        homeDataCache.put(cacheId, data);
                        deferred.resolve(data);

                    }, function (err) {
                        deferred.reject(err);
                    })
            }


            //return $http.get(API_URL + mainCategory + '/' + id + '?api_key=' + API_KEY);
        }

    }]);