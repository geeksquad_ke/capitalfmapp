angular.module('home')
    .controller('HomeSingleController', ['$scope', 'HomeService', '$ionicLoading', '$stateParams', '$ionicPopup', '$ionicNavBarDelegate', function ($scope, HomeService, $ionicLoading, $stateParams, $ionicPopup, $ionicNavBarDelegate) {

        init();

        function init() {

            $ionicLoading.show({
                template: 'Loading...'
            });

            HomeService.getFeeds($stateParams.maincategory, $stateParams.subcategory)
                .then(function (response) {
                    $scope.post = response.data[$stateParams.index];
                    $ionicLoading.hide();
                }, function (error) {

                    var showAlert = function () {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Error Occured',
                            template: 'The information is not available or you do not have a working internet connection'
                        });

                        alertPopup.then(function (res) {
                            $ionicNavBarDelegate.back();
                        });
                    };

                    $ionicLoading.hide();
                    showAlert();
                })

        }

    }]);
