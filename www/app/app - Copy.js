// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('capitalfm', [
    'ionic',
    'ionic.service.core',
    'ionic.service.analytics',
    'truncate',
    'ngCordova',
    'angular-cache',
    'user',
    'app',
    'home',
    'listen',
    'tv',
    'news',
    'saved',
    'jobs',
    'talk',
    'settings',
    'alerts',
    'mixes'
])


    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        // Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;

        // Remove the header used to identify ajax call  that would prevent CORS from working
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $urlRouterProvider.otherwise('/app/home/maincategories');

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/app/appmenu.html',
                controller: 'AppController'
            })
            .state('app.home', {
                url: '/home',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/home/home.html',
                        controller: 'HomeController'
                    }
                }
            })
            .state('app.home.maincategories', {
                url: '/maincategories',
                templateUrl: 'app/home/homemaincategories.html',
                controller: 'HomeController'
            })
            .state('app.home.subcategories', {
                url: '/:maincategory/:subcategory',
                templateUrl: 'app/home/homesubcategories.html',
                controller: 'HomeSubController'
            })
            .state('app.home.post', {
                url: '/:maincategory/:subcategory/:index',
                templateUrl: 'app/home/homesingle.html',
                controller: 'HomeSingleController'
            })
            .state('app.listen', {
                url: '/listen',
                views: {
                    maincontent: {
                        templateUrl: 'app/listen/listen.html',
                        controller: 'ListenController'
                    }
                }
            })
            .state('app.tv', {
                url: '/tv',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/tv/tv.html',
                        controller: 'TvController'
                    }
                }
            })
            .state('app.tv.main', {
                url: '',
                templateUrl: 'app/tv/tvmain.html',
                controller: 'TvController'
            })
            .state('app.tv.live', {
                url: '/live',
                templateUrl: 'app/tv/tvlive.html',
                controller: 'TvController'
            })
            .state('app.tv.new', {
                url: '/new',
                templateUrl: 'app/tv/tvnew.html',
                controller: 'TvController'
            })
            .state('app.tv.musicvideos', {
                url: '/musicvideos',
                templateUrl: 'app/tv/tvmusicvideos.html',
                controller: 'TvController'
            })
            .state('app.tv.shows', {
                url: '/shows',
                templateUrl: 'app/tv/tvshows.html',
                controller: 'TvController'
            })
            .state('app.tv.more', {
                url: '/more',
                abstract: true,
                templateUrl: 'app/tv/tvmore.html',
                controller: 'TvController'
            })
            .state('app.tv.more.main', {
                url: '',
                templateUrl: 'app/tv/tvmoremain.html',
                controller: 'TvController'
            })
            .state('app.tv.more.koroga', {
                url: '/koroga',
                templateUrl: 'app/tv/tvmoresingle.html',
                controller: 'TvController'
            })
            .state('app.tv.upload', {
                url: '/upload',
                templateUrl: 'app/tv/tvupload.html',
                controller: 'TvController'
            })
            .state('app.news', {
                url: '/news',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/news/news.html',
                        controller: 'NewsController'
                    }
                }
            })
            .state('app.news.list', {
                url: '',
                templateUrl: 'app/news/newslist.html',
                controller: 'NewsController'
            })
            .state('app.news.single', {
                url: '/:id',
                templateUrl: 'app/news/newssingle.html',
                controller: 'NewsController'
            })
            .state('app.saved', {
                url: '/saved',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/saved/saved.html',
                        controller: 'SavedController'
                    }
                }
            })
            .state('app.saved.list', {
                url: '',
                templateUrl: 'app/saved/savedlist.html',
                controller: 'SavedController'
            })
            .state('app.saved.single', {
                url: '/:id',
                templateUrl: 'app/saved/savedsingle.html',
                controller: 'SavedController'
            })
            .state('app.jobs', {
                url: '/jobs',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/jobs/jobs.html',
                        controller: 'JobsController'
                    }
                }
            })
            .state('app.jobs.list', {
                url: '',
                templateUrl: 'app/jobs/jobslist.html',
                controller: 'JobsController'
            })
            .state('app.jobs.single', {
                url: '/:id',
                templateUrl: 'app/jobs/job.html',
                controller: 'JobsController'
            })
            .state('app.alerts', {
                url: '/alerts',
                views: {
                    maincontent: {
                        templateUrl: 'app/alerts/alerts.html',
                        controller: 'AlertsController'
                    }
                }
            })
            .state('app.talk', {
                url: '/talk',
                views: {
                    maincontent: {
                        templateUrl: 'app/talk/talk.html',
                        controller: 'TalkController'
                    }
                }
            })
            .state('app.settings', {
                url: '/settings',
                views: {
                    maincontent: {
                        templateUrl: 'app/settings/settingscustomize.html',
                        controller: 'SettingsController'
                    }
                }
            })
            //.state('app.settings.main', {
            //    url: '',
            //    templateUrl: 'app/settings/settingsmain.html',
            //    controller: 'SettingsController'
            //})
            //.state('app.settings.account', {
            //    url: '/account',
            //    templateUrl: 'app/settings/settingsaccount.html',
            //    controller: 'SettingsController'
            //})
            //.state('app.settings.customize', {
            //    url: '/customize',
            //    templateUrl: 'app/settings/settingscustomize.html',
            //    controller: 'SettingsController'
            //})
            .state('app.mixes', {
                url: '/mixes',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/mixes/mixes.html',
                        controller: 'MixesController'
                    }
                }
            })
            .state('app.mixes.list', {
                url: '',
                templateUrl: 'app/mixes/mixeslist.html',
                controller: 'MixesController'
            })
            .state('app.mixes.single', {
                url: '/:id',
                templateUrl: 'app/mixes/mix.html',
                controller: 'MixController'
            })
            .state('app.user', {
                url: '/user',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/user/user.html',
                        controller: 'UserController'
                    }
                }

            })
            .state('app.user.signup', {
                url: '/signup',
                templateUrl: 'app/user/signup.html',
                controller: 'UserController'
            })
            .state('app.user.signin', {
                url: '/signin',
                templateUrl: 'app/user/signin.html',
                controller: 'UserController'
            })
    })


    .run(function ($ionicPlatform, $state, $rootScope, $http, UserService) {


        $rootScope.currentUser = {
            data: {}
        };

        Parse.initialize("capitalfm", "p0o9i8u7y6t5r4e3w2q1");

        Parse.serverURL = 'https://app.mymangamedia.com/v1';


        $rootScope.$on('$stateChangeStart', function (event, next, current) {

            $rootScope.currentUser.data = Parse.User.current();

        });


        $ionicPlatform.ready(function () {

            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);


            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }


            //Countly
            app = {};
            document.addEventListener("deviceready", function () {
                Countly.init("https://stats.mymangamedia.com", "dca4abeba667786492eb54af129b4991ea03d1df");
                //replace your server ip/url // replace it with your "App Key"
                // app.onRegistrationId();
                // app.test();
                // alert("deviceready");
                Countly.start();
                Countly.setLoggingEnabled();
            }, false);
            //Coutly

            //Parse
            
            var push = PushNotification.init({
                "android": {
                    "senderID": "968756748366",
                    "iconColor": "red",
                    "vibrate": true
                },
                "ios": {
                    "alert": true,
                    "badge": true,
                    "sound": true
                },
                "windows": {}
            });

            push.on('registration', function (data) {
                var token = data.registrationId;
                // coreservice.deviceToken = token; //comment it out for similar service like aproach
                var reqData = {
                    "deviceType": "android",
                    "deviceToken": token,
                    "pushType": "gcm",
                    "GCMSenderId": "968756748366",
                    "appName": "CapitalFM",
                    "appVersion": "1.1.4",
                    "channels": ["news", "sports", "lifestye", "business"]

                }
                return $http.post('https://app.mymangamedia.com/v1/installations', reqData, {
                    "headers": {
                        "Content-Type": "application/json",
                        "X-Parse-Application-Id": "capitalfm",
                        "X-Parse-REST-API-Key": "p0o9i8u7y6t5r4e3w2q1"
                    }
                })
                    .success(function (res, status, headers, config) {
                        console.log("Registering NEW device successfull");
                    })
                    .error(function (res, status, headers, config) {
                        console.log("Something went wrong");
                    });
            })
            
            //Parse


            //Apprate
            AppRate.preferences.storeAppURL = {ios: '1118910512',
                                               android: 'market://details?id=co.ke.geeksquad.capitalfm',
                                            };
            AppRate.preferences.openStoreInApp = true;
            AppRate.preferences.displayAppName = 'Capital FM';
            AppRate.preferences.usesUntilPrompt = 10;
            AppRate.preferences.promptAgainForEachNewVersion = false;
            AppRate.promptForRating(false);
            //Apprate


        });


    })

    .constant('API_URL', 'https://www.capitalfm.co.ke/')
    .constant('API_KEY', 'b1c824b5ba52db05397668fe4d949093329aa8bf31aa71c42de6e8e6e9fadc01')
    .constant('SOUNDCLOUD_USERNAME', '984capitalfm')

