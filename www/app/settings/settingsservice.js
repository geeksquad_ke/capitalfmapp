angular.module('settings')
    .factory('SettingsService', [function () {

        return {
            getSettings :  getSettings,
            updateSettings: updateSettings
        };

        function getSettings(){

            var currentUser = Parse.User.current();


            var Preferences = Parse.Object.extend("prefs");

            var query = new Parse.Query(Preferences);

            query.equalTo("user", currentUser);

            return query.find();



        }

        function updateSettings(settings) {

            var currentUser = Parse.User.current();


            var Preferences = Parse.Object.extend("prefs");

            var query = new Parse.Query(Preferences);

            query.equalTo("user", currentUser);

            return query.find({
                success: function (results) {

                    if (results.length >= 1) {
                        var currentSettings = results[0];

                        currentSettings.set("business", settings.channels.business);
                        currentSettings.set("news", settings.channels.news);
                        currentSettings.set("lifestyle", settings.channels.lifestyle);
                        currentSettings.set("sports", settings.channels.sports);
                        currentSettings.set("mixes", settings.channels.mixes);
                        currentSettings.set("alerts", settings.channels.alerts);

                        currentSettings.save();
                    }
                    else {

                        var currentSettings = new Preferences();

                        currentSettings.set("business", settings.channels.business);
                        currentSettings.set("news", settings.channels.news);
                        currentSettings.set("lifestyle", settings.channels.lifestyle);
                        currentSettings.set("sports", settings.channels.sports);
                        currentSettings.set("mixes", settings.channels.mixes);
                        currentSettings.set("alerts", settings.channels.alerts);
                        currentSettings.set("user", currentUser);

                        currentSettings.save();

                    }

                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });



        }
    }]);