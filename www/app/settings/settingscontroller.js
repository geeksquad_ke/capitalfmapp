angular.module('settings')

    .controller('SettingsController', ['$scope', 'SettingsService', '$ionicLoading', '$ionicPopup', '$state', function ($scope, SettingsService, $ionicLoading, $ionicPopup, $state) {

        $scope.settings = {
            "channels": {
                "news": true,
                "business": true,
                "sports": true,
                "lifestyle": true,
                "mixes": true,
                "alerts": true
            }
        };

        if (Parse.User.current()) {

            init();

        }


        function init() {

            SettingsService.getSettings()
                .then(function (results) {

                    $ionicLoading.show({
                        template: "Fetching Settings..."
                    });

                    if (results.length >= 1) {
                        var savedSettings = results[0].attributes;
                        $scope.settings.channels = JSON.parse(JSON.stringify(savedSettings));
                        console.log($scope.settings.channels);
                        $ionicLoading.hide();
                    }

                });
        }

        $scope.updateSettings = function () {

            if (!Parse.User.current()) {


                var alertPopup = $ionicPopup.alert({
                    title: 'Please Login or Sign Up',
                    template: 'To save your settings'
                });

                alertPopup.then(function (res) {
                    $state.go('app.user.signin');
                });


            }
            else {
                $ionicLoading.show({
                    template: "Updating Settings..."
                });


                SettingsService.updateSettings($scope.settings)
                    .then(function (response) {
                        $ionicLoading.hide();
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                    })

            }
            ;
        }

    }]);