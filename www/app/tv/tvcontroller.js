angular.module('tv')
    .controller('TvController',['$scope', '$http', '$ionicLoading', function($scope, $http, $ionicLoading){


        $scope.videos = [];

        $scope.playerVars = {
            rel: 0,
            showinfo: 0,
            modestbranding: 0
        };

        init();

        function init(){

            $ionicLoading.show({
                template: 'Loading...'
            });

            $scope.youtubeParams = {
                key: 'AIzaSyAH0lctEScLEl-qUrE3jgB6n_11IPrVG1k',
                type: 'video',
                maxResults: '10',
                part: 'id,snippet',
                q: '',
                order: 'date',
                channelId: 'UCsURxs8Kz_qzezpicj-STyw'
            };


            $http.get('https://www.googleapis.com/youtube/v3/search', {params:$scope.youtubeParams}).success(function(response){
                $ionicLoading.hide();
                angular.forEach(response.items, function(child){
                    $scope.videos.push(child)
                });
            });
        }


        $scope.loadVideos = function() {
            for(var i = 0; i < 10; i++) {
                //$scope.images.push({id: i, src: "http://placehold.it/200x150"});
                $scope.images.push({id: i, src: "http://placekitten.com/g/200/150"});
            }
        };

        $scope.shouldShowDelete = true;
        $scope.shouldShowReorder = true;
        $scope.listCanSwipe = true;
    }])