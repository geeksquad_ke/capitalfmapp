angular.module('listen')
    .controller('ListenController', ['$scope','$interval', 'ListenService', function ($scope, $interval, ListenService) {


        var isPlaying = false;
        var stream;
        var timer;

            $scope.togglePlay = togglePlay;
            $scope.isPlaying = isPlaying;
            $scope.info = null;

// *********************************************************************
        function togglePlay() {
            if ($scope.isPlaying) {
                pause();
            } else {
                play();
            }
            $scope.isPlaying = isPlaying = !isPlaying;
        }

        function play() {
            if (window.Stream) {
                stream = new window.Stream('https://live.mymangamedia.com:443/');
                // Play audio
                stream.play();
            }
            getStreamInfo();
            timer = $interval(function() {
                getStreamInfo();
            }, 5000);
        }

        function pause() {
            $scope.info = null;
            $interval.cancel(timer);
            if (!stream) {
                return;
            }
            stream.stop();
        }

        function getStreamInfo() {
            ListenService.getStreamInfo().then(function(info) {
                $scope.info = info;
            }, function() {
                $scope.info = null;
            });
        }

    }]);