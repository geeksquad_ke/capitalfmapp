angular.module('user')
.factory('UserService',[function(){

        return {
            getCurrentUser : getCurrentUser
        };

        function getCurrentUser(){

            return Parse.User.current();
        }

    }]);