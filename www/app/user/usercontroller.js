angular.module('user')
    .controller('UserController', ['$scope', '$state', '$ionicPopup', '$timeout', '$rootScope', 'UserService', function ($scope, $state, $ionicPopup, $timeout, $rootScope, UserService) {


        $scope.data = {};

        $scope.signupEmail = function () {
            //Create a new user on Parse
            var user = new Parse.User();
            user.set("fn", $scope.data.fname);
            user.set("ln", $scope.data.lname);
            user.set("mobile", $scope.data.mobile);
            user.set("username", $scope.data.username);
            user.set("password", $scope.data.password);
            user.set("email", $scope.data.email);

            user.signUp(null, {
                success: function (user) {
                    // Hooray! Let them use the app now.


                        $scope.data = {};

                        // An elaborate, custom popup
                        var myPopup = $ionicPopup.show({
                            title: 'Welcome',
                            subTitle: 'You have been Signed Up',
                            scope: $scope
                        });



                        $timeout(function () {
                            myPopup.close(); //close the popup after 3 seconds for some reason
                        }, 3000);



                    $state.go('app.home.maincategories');

                },
                error: function (user, error) {
                    // Show the error message somewhere and let the user try again.
                    alert("Error: " + error.code + " " + error.message);
                }
            });


        };

        $scope.loginEmail = function () {
            //Login user to Parse
            Parse.User.logIn($scope.data.username, $scope.data.password, {
                success: function (user) {
                    // Do stuff after successful login.


                    $scope.data = {};

                    // An elaborate, custom popup
                    var myPopup = $ionicPopup.show({
                        title: 'Welcome',
                        subTitle: 'You have been logged in',
                        scope: $scope
                    });

                    myPopup.then(function (res) {
                        $state.go('app.home.maincategories');
                    });

                    $timeout(function () {
                        myPopup.close(); //close the popup after 3 seconds for some reason
                    }, 2000);


                },
                error: function (user, error) {
                    // The login failed. Check error to see why.
                    var loginFailedPopup = $ionicPopup.alert({
                        title: 'Login Failed',
                        template: 'Check your credentials or internet connection then try again'
                    });

                    loginFailedPopup.then(function(res) {

                    });
                }
            });

        };

    }]);