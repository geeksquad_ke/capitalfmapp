// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('capitalfm', [
    'ionic',
    'ionic.service.core',
    'ionic.service.analytics',
    'app',
    'home',
    'listen',
    'tv',
    'news',
    'saved',
    'jobs',
    'talk',
    'settings',
    'alerts'
])


    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/app/home');

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/app/appmenu.html',
                controller: 'AppController'
            })

            .state('app.home', {
                url: '/home',
                views: {
                    maincontent: {
                        templateUrl: 'app/home/home.html',
                        controller: 'HomeController'
                    }
                }
            })

            .state('app.listen', {
                url: '/listen',
                views: {
                    maincontent: {
                        templateUrl: 'app/listen/listen.html',
                        controller: 'ListenController'
                    }
                }
            })

            .state('app.tv', {
                url: '/tv',
                abstract: 'true',
                views: {
                    maincontent: {
                        templateUrl: 'app/tv/tv.html',
                        controller: 'TvController'
                    }
                }
            })

            .state('app.tv.main', {
                url: '',
                templateUrl: 'app/tv/tvmain.html',
                controller: 'TvController'
            })

            .state('app.tv.live', {
                url: '/live',
                templateUrl: 'app/tv/tvlive.html',
                controller: 'TvController'
            })

            .state('app.tv.new', {
                url: '/new',
                templateUrl: 'app/tv/tvnew.html',
                controller: 'TvController'
            })

            .state('app.tv.musicvideos', {
                url: '/musicvideos',
                templateUrl: 'app/tv/tvmusicvideos.html',
                controller: 'TvController'
            })


            .state('app.tv.shows', {
                url: '/shows',
                templateUrl: 'app/tv/tvshows.html',
                controller: 'TvController'
            })


            .state('app.tv.more', {
                url: '/more',
                abstract: true,
                templateUrl: 'app/tv/tvmore.html',
                controller: 'TvController'
            })

            .state('app.tv.more.main', {
                url: '',
                templateUrl: 'app/tv/tvmoremain.html',
                controller: 'TvController'
            })

            .state('app.tv.more.koroga', {
                url: '/koroga',
                templateUrl: 'app/tv/tvmoresingle.html',
                controller: 'TvController'
            })

            .state('app.tv.upload', {
                url: '/upload',
                templateUrl: 'app/tv/tvupload.html',
                controller: 'TvController'
            })


            .state('app.news', {
                url: '/news',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/news/news.html',
                        controller: 'NewsController'
                    }
                }
            })

            .state('app.news.list', {
                url: '',
                templateUrl: 'app/news/newslist.html',
                controller: 'NewsController'
            })


            .state('app.news.single', {
                url: '/:id',
                templateUrl: 'app/news/newssingle.html',
                controller: 'NewsController'
            })

            .state('app.saved', {
                url: '/saved',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/saved/saved.html',
                        controller: 'SavedController'
                    }
                }
            })

            .state('app.saved.list', {
                url: '',
                templateUrl: 'app/saved/savedlist.html',
                controller: 'SavedController'
            })

            .state('app.saved.single', {
                url: '/:id',
                templateUrl: 'app/saved/savedsingle.html',
                controller: 'SavedController'
            })

            .state('app.jobs', {
                url: '/jobs',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/jobs/jobs.html',
                        controller: 'JobsController'
                    }
                }
            })

            .state('app.jobs.list', {
                url: '',
                templateUrl: 'app/jobs/jobslist.html',
                controller: 'JobsController'

            })

            .state('app.jobs.single', {
                url: '/:id',

                templateUrl: 'app/jobs/job.html',
                controller: 'JobsController'

            })

            .state('app.alerts', {
                url: '/alerts',
                views: {
                    maincontent: {
                        templateUrl: 'app/alerts/alerts.html',
                        controller: 'AlertsController'
                    }
                }
            })

            .state('app.talk', {
                url: '/talk',
                views: {
                    maincontent: {
                        templateUrl: 'app/talk/talk.html',
                        controller: 'TalkController'
                    }
                }
            })

            .state('app.settings', {
                url: '/settings',
                abstract: true,
                views: {
                    maincontent: {
                        templateUrl: 'app/settings/settings.html'
                    }
                }

            })

            .state('app.settings.main', {
                url: '',
                templateUrl: 'app/settings/settingsmain.html'
            })


            .state('app.settings.account', {
                url: '/account',
                templateUrl: 'app/settings/settingsaccount.html'

            })


            .state('app.settings.customize', {
                url: '/customize',
                templateUrl: 'app/settings/settingscustomize.html'

            })


    })

    .run(function($ionicPlatform, $ionicAnalytics) {
  $ionicPlatform.ready(function() {

            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            //Ionic Analytics
            $ionicAnalytics.register();

            //Parse Push
            var push = PushNotification.init({
      android: {
          senderID: "968756748366"
      },
      ios: {
          alert: true,
          badge: true,
          sound: false
      },
      windows: {}
    });

push.on('registration', function(data) {
      User.pushInstallation(platform, data.registrationId);
})

            var parseInstallationId = Parse._getInstallationId().then(function(inId){

      var d = {
        "deviceType": platform,  
        "deviceToken": registrationId,
        "channels": ["news","business","lifestyle","sports","mixes"],
        "installationId":inId
      }
      var userId = Parse.User.current().id; 
      d['user'] = {
        "__type": "Pointer",
        "className": "_User",
        "objectId": userId
      }
      if(platform === 'android'){
        d.pushType = 'gcm';
      }

      $http({
        method: 'POST',
        url: 'https://app.mymangamedia.com/v1/installations/',
        headers: {'X-Parse-Application-Id':'capitalfm','X-Parse-REST-API-Key':'p0o9i8u7y6t5r4e3w2q1'},
        data: d
      }).then(function successCallback(response) {
          installationId = response;
        }, 
        function errorCallback(response) {
          console.log('pushInstallation error: ', response);
      });
    })

            //Parse Push


            //OneSignal
            document.addEventListener('deviceready', function () {
  // Enable to debug issues.
  // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

  var notificationOpenedCallback = function(jsonData) {
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("8bb64980-0835-4f36-8d8f-e84fb00db85f",
                                 {googleProjectNumber: "968756748366"},
                                 notificationOpenedCallback);

  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(true);
}, false);



            });


         })