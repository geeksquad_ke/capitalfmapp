angular.module('mixes')
    .controller('MixesController', ['$scope', 'MixesService', '$ionicLoading', '$sce', function ($scope, MixesService, $ionicLoading, $sce) {

        init();

        console.log('Mixes Controller Called');
        function init() {

            console.log('Init Called');

            $ionicLoading.show({
                template: 'Loading...'
            });

            MixesService.getUserMixes()
                .then(function (responses) {
                    $scope.mixes = responses.data;
                    console.log($scope.mixes);
                    $ionicLoading.hide();
                })
        }


        $scope.play = function(track_url){

            SC.oEmbed(track_url, { auto_play: true }, function(oEmbed) {
                $scope.$apply($scope.player_html = $sce.trustAsHtml(oEmbed.html));
            });

        }

    }]);