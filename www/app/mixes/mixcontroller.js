angular.module('mixes')
    .controller('MixController', ['$scope', 'MixesService', '$ionicLoading', '$sce', '$stateParams', function ($scope, MixesService, $ionicLoading, $sce, $stateParams) {

        var track_id = $stateParams.id;

        var track_url = 'https://api.soundcloud.com/tracks/' + track_id;

        play();


        $ionicLoading.show({
            template: 'Loading...'
        });


        function play () {

            SC.oEmbed(track_url, {auto_play: true}, function (oEmbed) {
                $scope.$apply($scope.player_html = $sce.trustAsHtml(oEmbed.html));
                $ionicLoading.hide();
            });

        }

    }]);