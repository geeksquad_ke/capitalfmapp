angular.module('mixes')
.factory('MixesService',['$http', 'API_KEY', 'SOUNDCLOUD_USERNAME',function($http, API_KEY, SOUNDCLOUD_USERNAME ){

        return {
            getMixes : getMixes,
            getUserMixes : getUserMixes
        }


        function getMixes(){
            return $http.get('https://api.mymangamedia.com/api/v2/mixes' +'?api_key=' + API_KEY);
        }

        function getUserMixes(){

            return $http.get('https://api.soundcloud.com/users/' + SOUNDCLOUD_USERNAME + '/tracks.json?client_id=595466ed0b0851288f275d1ce633fa7c');

        }
    }]);