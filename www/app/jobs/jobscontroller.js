angular.module('jobs', [])

    .controller('JobsController', [ '$scope',function ($scope) {

        $scope.jobs = [
            {
                id : '1',
                title: 'Marketing Head',
                description: "You will be the head of Marketing"
            },
            {
                id : '2',
                title: 'Sales Rep',
                description: "Sell and Market Our products"
            },
            {
                id : 3,
                title: 'Radio Host',
                description: "Become one of the radio hosts"
            }
        ]

    }])