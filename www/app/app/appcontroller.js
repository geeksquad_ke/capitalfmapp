angular.module('app')
    .controller('AppController', ['$scope', '$ionicSideMenuDelegate', '$state', function ($scope, $ionicSideMenuDelegate, $state) {


        $scope.toggleLeft = function () {
            $ionicSideMenuDelegate.toggleLeft();

        };

        $scope.logoutUser = function () {

            Parse.User.logOut()
                .then(function(response){

                    //$state.go('app.home.maincategories');

                    $state.go($state.current, {}, {reload: true});

                });


        };


        //Ionic Deploy
        var deploy = new Ionic.Deploy();

        // Update app code with new release from Ionic Deploy
        $scope.doUpdate = function () {
            deploy.update().then(function (res) {
                console.log('Ionic Deploy: Update Success! ', res);
            }, function (err) {
                console.log('Ionic Deploy: Update error! ', err);
            }, function (prog) {
                console.log('Ionic Deploy: Progress... ', prog);
            });
        };

        // Check Ionic Deploy for new code
        $scope.checkForUpdates = function () {
            console.log('Ionic Deploy: Checking for updates');
            deploy.check().then(function (hasUpdate) {
                console.log('Ionic Deploy: Update available: ' + hasUpdate);
                $scope.hasUpdate = hasUpdate;
            }, function (err) {
                console.error('Ionic Deploy: Unable to check for updates', err);
            });
        };


    }]);